package com.jose.samplecode.model

data class Contact(
    val name: String, val id: Long, val companyName: String,
    val isFavorite: Boolean, val smallImageURL: String, val largeImageURL: String,
    val emailAddress: String, val birthdate: String, val phone: Phone,
    val address: Address
)


data class Phone(val work: String, val home: String, val mobile: String)

data class Address(val street: String, val city: String, val state: String, val country: String, val zipCode: String)