package com.jose.samplecode.repository

import com.jose.samplecode.model.Contact
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import io.reactivex.subjects.BehaviorSubject

class AWSService {

    var subjectListOfContacts = BehaviorSubject.create<List<Contact>>()

    fun getListOfContacts(): BehaviorSubject<List<Contact>?> {
        val retrofit1 = Retrofit.Builder()
            .baseUrl(SERVICE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit1.create(AWSClient::class.java)
        val jsonCall = service.readJson()
        jsonCall.enqueue(object : Callback<List<Contact>> {
            override fun onResponse(call: Call<List<Contact>>, response: Response<List<Contact>>) {
                subjectListOfContacts.onNext(response.body()!!)
            }

            override fun onFailure(call: Call<List<Contact>>, t: Throwable) {
                //TODO
            }
        })

        return subjectListOfContacts
    }

    companion object {
        const val SERVICE_URL = "https://s3.amazonaws.com/technical-challenge/v3/"
    }

}