package com.jose.samplecode.repository

import com.jose.samplecode.model.Contact
import io.reactivex.subjects.BehaviorSubject

class ContactRepository {

    fun getListOfContactObservable(): BehaviorSubject<List<Contact>?> {
        return AWSService().getListOfContacts()
    }

}