package com.jose.samplecode.repository

import com.jose.samplecode.model.Contact

import retrofit2.http.GET
import retrofit2.Call

interface AWSClient {

    @GET("contacts.json")
    fun readJson(): Call<List<Contact>>

}