package com.jose.samplecode.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jose.samplecode.R
import com.jose.samplecode.model.Contact
import kotlinx.android.synthetic.main.contact_list_item.view.*

class ContactAdapter(private val list: List<Contact>) : RecyclerView.Adapter<CompanyViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        context = parent.context
        return CompanyViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) {
        val contact: Contact = list[position]
        holder.bind(contact)
    }

    override fun getItemCount(): Int = list.size

}

class CompanyViewHolder(inflater: LayoutInflater, private var parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.contact_list_item, parent, false)) {

    private var contactNameTextView: TextView? = null
    private var companyNameTextView: TextView? = null
    private var favoriteImageView: ImageView? = null

    init {
        contactNameTextView = itemView.findViewById(R.id.contact_name)
        companyNameTextView = itemView.findViewById(R.id.company_name)
        favoriteImageView = itemView.findViewById(R.id.favorite_icon)
    }

    fun bind(contact: Contact) {
        contactNameTextView?.text = contact.name
        companyNameTextView?.text = contact.birthdate

        if (contact.isFavorite) {
            favoriteImageView?.visibility = View.VISIBLE
        }

        Glide
            .with(parent.context)
            .load(contact.smallImageURL)
            .into(itemView.contact_image)
    }

}