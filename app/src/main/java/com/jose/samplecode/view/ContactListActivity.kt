package com.jose.samplecode.view

class ContactListActivity : SingleFragmentActivity() {

    override fun createFragment() = ContactListFragment.newInstance()

}