package com.jose.samplecode.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jose.samplecode.R
import com.jose.samplecode.repository.AWSService
import com.jose.samplecode.viewModel.ContactViewModel
import kotlinx.android.synthetic.main.activity_contact_list.*

class ContactListFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_contact_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            ContactViewModel().onLoadContactsInformation().subscribe {
                adapter = ContactAdapter(it!!)
            }

        }
    }

    companion object {
        fun newInstance(): ContactListFragment = ContactListFragment()
    }

}