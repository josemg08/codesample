package com.jose.samplecode.viewModel

import com.jose.samplecode.model.Contact
import com.jose.samplecode.repository.ContactRepository
import io.reactivex.subjects.BehaviorSubject

class ContactViewModel {

    var subjectListOfContacts = BehaviorSubject.create<List<Contact>>()

    fun onLoadContactsInformation(): BehaviorSubject<List<Contact>?> {
        ContactRepository().getListOfContactObservable().subscribe {
            subjectListOfContacts.onNext(it!!)
        }
        return subjectListOfContacts
    }

}